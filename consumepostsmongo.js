const URL = "https://api.mlab.com/api/1/databases/bootcampijavier/collections/posts?apiKey=9WUP-KKUOcoAsrAa_2eufBQItHAYyJdk";
var response;

function obtenerPosts() {
  var peticion = new XMLHttpRequest();
  peticion.open("GET", URL, false);
  peticion.setRequestHeader("Content-type","application/json");
  peticion.send();
  sessionStorage["posts"] = peticion.responseText;
  console.log(response);
  //mostrarPosts();
};

function mostrarPosts(){
  var tabla = document.getElementById("tablaPost");

  for (var i = 0;i < response.length; i++){
      var fila = tabla.insertRow(i+1);
      var celdaId = fila.insertCell(0);
      var celdaTitulo = fila.insertCell(1);
      var celdaTexto = fila.insertCell(2);
      var celdaAutor = fila.insertCell(3);
      var celdaOperaciones = fila.insertCell(4);

      celdaId.innerHTML = response[i]._id.$oid;
      celdaTitulo.innerHTML = response[i].titulo;
      celdaTexto.innerHTML = response[i].texto;
      if (response[i].autor != undefined)
        celdaAutor.innerHTML = response[i].autor.nombre + " " + response[i].autor.apellido;
      else{
        celdaAutor.innerHTML = "Anónimo" ;
      }
      celdaOperaciones.innerHTML = '<button onclick=\'actualizarPost("' + celdaId.innerHTML + '")\';>Actualizar</button>'
  }
};

function anadirPost() {
  var peticion = new XMLHttpRequest();
  peticion.open("POST", URL, false);
  peticion.setRequestHeader("Content-Type", "application/json");
  peticion.send('{"titulo":"Nuevo POST desde Atom", "texto":"Nuevo texto desde ATOM", "autor":{"nombre":"Fernando", "apellido":"Minguero"}}');
};

function actualizarPost(id) {
  var peticion = new XMLHttpRequest();
  var URLItem = "https://api.mlab.com/api/1/databases/bootcampijavier/collections/posts/";
  URLItem += id;
  URLItem += "?apiKey=9WUP-KKUOcoAsrAa_2eufBQItHAYyJdk";
  peticion.open("PUT", URLItem, false);
  peticion.setRequestHeader("Content-Type","application/json");
  peticion.send('{"titulo":"Titulo cambiado"}');
};

function seleccionarPost(numero){
  sessionStorage["seleccionado"] = numero;
};
