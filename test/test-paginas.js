var expect = require('chai').expect;
var request = require('request');
var $ = require('chai-jquery');

it('Test suma', function() {
  expect(9+4).to.equal(13);
});

it('Test internet', function(done) {
  request.get("http://www.google.es", function(error,response,body){
    expect(response.statusCode).to.equal(200);
    done();
  });
});

it('Test servicio levantado local', function(done) {
  request.get("http://localhost:8081", function(error,response,body){
    expect(response.statusCode).to.equal(200);
    done();
  });
});

describe("Test contenido html", function () {
  it('Test H1', function () {
    request.get("http://localhost:8081", function (error, res, body) {
      console.log(body);
      expect($('body h1')).to.have.text("Bienvenido a mi blog");
    });
  });
});
